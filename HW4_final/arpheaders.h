#include "hw_addrs.h"
#include <stdio.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/if_arp.h>
#include <sys/un.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <wait.h>



#define USID_PROTO 0x77A5
#define ARP_PROTO 0x77A5
#define FRAME_TYPE 0x0806
#define HARD_TYPE 1
#define PROTO_TYPE 0x800
#define HARD_SIZE 6
#define PROTO_SIZE 4
#define REQ_OP 1
#define RES_OP 2
#define ARP_PATH "arpfile"
#define MAXLINE 1024


struct arp_packet{
	uint16_t hard_type;
	uint16_t proto_type;
	uint8_t hard_size;
	uint8_t proto_size;
	uint16_t operation;
	uint16_t proto_num;
	unsigned char sender_eth_addr[6];
	in_addr_t sender_ip_addr;
	unsigned char target_eth_addr[6];
	in_addr_t target_ip_addr;
};

struct myaddress{
	in_addr_t my_ip_addr;
	unsigned char my_hw_addr[6];
	struct myaddress *next;
};


struct mycache{
	in_addr_t ip_addr;
	unsigned char hw_addr[6];
	int sll_ifiindex;
	uint8_t sll_hatype;
	int sockfd;
	struct mycache *next;
};

struct hwaddr {
	int             sll_ifindex;	 /* Interface number */
	unsigned short  sll_hatype;	 /* Hardware type */
	unsigned char   sll_halen;	/* Length of address */
	unsigned char   sll_addr[8];	 /* Physical layer address */
};
