#include "arpheaders.h"
#define IPPROTO_TOUR 245
#define LISTMAX 128
#define MULTICAST_ADDR "225.0.0.37"
#define MULTICAST_PORT 11790
#define FRAME_SIZE 98ount
#define LOCAL_ID 0x77A5
#define TTL_OUT 64

int first_visit = 1;
int m_sock, m_readsock, pg_sock;
int second_multicast = 0;
int counter = 0;
int stop_pinging = 0;
int pingcount =0;
char myName[INET_ADDRSTRLEN];

pthread_mutex_t index_mutex = PTHREAD_MUTEX_INITIALIZER;

struct touraddress{
int no_of_nodes;
int curr_index;
int port;
in_addr_t multicast_address;
in_addr_t tour_address[LISTMAX];
};

struct myprevaddress{
	in_addr_t prev_ip_addr;
	struct myprevaddress *next;
};

struct touraddress *mycurrent_address;
struct myprevaddress *head;
struct myprevaddress *returnaddr;
// #define ETH_P_IP 0x0800

int return_vmname(in_addr_t ipaddress, char *str)
{
	struct hostent *hptr;

	memset(str, 0, INET_ADDRSTRLEN);
	if( (hptr = gethostbyaddr( (char *)&ipaddress,4,AF_INET)) < 0 ){
		printf("\nInvalid address");
	}
//	printf("\n vm name is %s", hptr->h_name);
	memcpy(str, hptr->h_name, INET_ADDRSTRLEN);
fflush(stdout);
return 0;
}

int add_myprevaddr(in_addr_t my_ip_addr, struct myprevaddress *head)
{
        if(!head->prev_ip_addr)
        {
                head->prev_ip_addr = my_ip_addr;
                head->next = NULL;
                return 1;
        }
        else
        {
                struct myprevaddress *newnode = (struct myprevaddress *)malloc(sizeof(struct myprevaddress));
                struct myprevaddress *temp;
                newnode->prev_ip_addr = my_ip_addr;
                newnode->next = NULL;
                temp = head;
                while(temp->next)
                {
                        temp= temp->next;
                }
                temp->next = newnode;
                return 1;
        }
        return 0;
}



struct myprevaddress *find_myprevaddr(in_addr_t my_ip_addr, struct myprevaddress *head)
{
        struct myprevaddress *temp;
	temp = head;
	while(temp)
        {
		if(temp->prev_ip_addr == my_ip_addr)
                {
                        return temp;
                }
                temp = temp->next;
	}

	return 0;
}


uint16_t
in_cksum(uint16_t *addr, int len)
{
        int                             nleft = len;
        uint32_t                sum = 0;
        uint16_t                *w = addr;
        uint16_t                answer = 0;

        /*
         * Our algorithm is simple, using a 32 bit accumulator (sum), we add
         * sequential 16 bit words to it, and at the end, fold back all the
         * carry bits from the top 16 bits into the lower 16 bits.
         */
        while (nleft > 1)  {
                sum += *w++;
                nleft -= 2;
        }

                /* 4mop up an odd byte, if necessary */
        if (nleft == 1) {
                *(unsigned char *)(&answer) = *(unsigned char *)w ;
                sum += answer;
        }

                /* 4add back carry outs from top 16 bits to low 16 bits */
        sum = (sum >> 16) + (sum & 0xffff);     /* add hi 16 to low 16 */
        sum += (sum >> 16);                     /* add carry */
        answer = ~sum;                          /* truncate to 16 bits */
        return(answer);
}


void mcast_send(char* msg)
{
        struct sockaddr_in mcast_addr;
	int n, mcast_send;
	mcast_send = socket(AF_INET, SOCK_DGRAM, 0);

	memset(&mcast_addr, 0, sizeof(mcast_addr));
	mcast_addr.sin_family = AF_INET;
	mcast_addr.sin_port = htons(MULTICAST_PORT);
	mcast_addr.sin_addr.s_addr = inet_addr(MULTICAST_ADDR);
	n = sendto(mcast_send, msg, MAXLINE, 0, (struct sockaddr *)&mcast_addr, sizeof(mcast_addr));
	if(n < 0){
		perror("\nsend");
		printf("\nMulticast send error\n");
	}

}


int join_multicast_group(int sockfd, in_addr_t mcast_ip_addr, int mport)
{
	int ttl_val = 1;
	struct ip_mreq mreq;

	/* use setsockopt() to request that the kernel join a multicast group */
	mreq.imr_multiaddr.s_addr= inet_addr(MULTICAST_ADDR);
	mreq.imr_interface.s_addr= htonl(INADDR_ANY);
	if (setsockopt(sockfd,IPPROTO_IP,IP_ADD_MEMBERSHIP,&mreq,sizeof(mreq)) < 0) {
		perror("setsockopt");
		return -1;
	}
	if (setsockopt(sockfd,IPPROTO_IP,IP_MULTICAST_TTL,&ttl_val,sizeof(ttl_val)) < 0) {
		perror("setsockopt");
		return -1;
	}
	first_visit = 0;
	return 0;
}

int areq(struct sockaddr *IPaddr, socklen_t sockaddrlen, struct hwaddr *HWaddr)
{
	int unx_sock;
	struct sockaddr_un cli_addr;
	char *buf;
	int n, length, ret = 0;

	fd_set setallfd;
	struct timeval tv;

	buf = malloc(sizeof(struct sockaddr) + sizeof(struct hwaddr));
	memcpy(buf, IPaddr, sockaddrlen);
	memcpy(buf + sockaddrlen, HWaddr, sizeof(struct hwaddr));

	unx_sock = socket(AF_LOCAL, SOCK_STREAM, 0);
	
	bzero(&cli_addr, sizeof(cli_addr));
	cli_addr.sun_family = AF_LOCAL;
	strcpy(cli_addr.sun_path, ARP_PATH);

	length = sizeof(cli_addr);
	if (connect(unx_sock, (struct sockaddr *)&cli_addr, sizeof(cli_addr)) < 0) {
		printf("Error in connecting to unix domain socket\n");
		return -1;
	}


	if(write(unx_sock, buf, sockaddrlen + sizeof(struct hwaddr)) < 0) { 
	 perror("error in write");
	  ret = -1;
	}

	tv.tv_sec = 15;
	tv.tv_usec = 0;
	FD_ZERO(&setallfd);
	FD_SET(unx_sock, &setallfd);	

	n = select(unx_sock + 1, &setallfd, NULL, NULL, &tv);
	if (n < 0) {
		printf("\nError with select listening\n");
		close(unx_sock);
		return -1; 
	}

	if(FD_ISSET(unx_sock, &setallfd)) {
		ret = read(unx_sock, HWaddr, sizeof(*HWaddr));
		if(ret <= 0) {
			perror("\nRead error:");
			close(unx_sock);
			return -1;
		}
        } 
	else {
		printf("\nTimeout occured while waiting for a reply from ARP.\n");
		close(unx_sock);		
		return -2;
	}

return 0;
}


int ping_prev_node()
{
    struct iphdr* ip;
    struct iphdr* ip_reply;
    struct icmphdr* icmp;
    struct sockaddr_in connection;
    char* packet;
    char* buffer;
    int optval;
    int addrlen;
    int siz;
	char dst_addr[20];
	char src_addr[20];
	struct timeval curr_time;
	
	strcpy(dst_addr, "192.168.1.109");
	strcpy(src_addr, "192.168.1.108");
	
    printf("Source address: %s\n", src_addr);
    printf("Destination address: %s\n", dst_addr);
	     
    /*
     * allocate all necessary memory
    */
    packet = (char *)malloc(sizeof(struct iphdr) + sizeof(struct icmphdr));
    buffer = (char *)malloc(sizeof(struct iphdr) + sizeof(struct icmphdr));
    /****************************************************************/
     
	//Both the headers
    ip = (struct iphdr*) packet;
    icmp = (struct icmphdr*) (packet + sizeof(struct iphdr));
     
    /* 
     *  here the ip packet is set up
     */
	ip->ihl          = 5;
    ip->version          = 4;
    ip->tos          = 0;
    ip->tot_len          = sizeof(struct iphdr) + sizeof(struct icmphdr);
    ip->id           = htons(LOCAL_ID);
    ip->frag_off     = 0;
    ip->ttl          = 64;
    ip->protocol     = IPPROTO_ICMP;
    ip->saddr            = inet_addr(src_addr);
    ip->daddr            = inet_addr(dst_addr);
    ip->check            = in_cksum((unsigned short *)ip, sizeof(struct iphdr));
     

     
    /*
     *  here the icmp packet is created
     *  also the ip checksum is generated
     */
    icmp->type           = ICMP_ECHO;
    icmp->code           = 0;
    icmp->un.echo.id     = htons(LOCAL_ID);
    icmp->un.echo.sequence   = 0;
    icmp-> checksum      = in_cksum((unsigned short *)icmp, sizeof(struct icmphdr));
     
     
    connection.sin_family = AF_INET;
    connection.sin_addr.s_addr = inet_addr(dst_addr);
     
    /*
     *  now the packet is sent
     */
     
    sendto(pg_sock, packet, ip->tot_len, 0, (struct sockaddr *)&connection, sizeof(struct sockaddr));
    printf("Sent %d byte packet to %s\n", ip->tot_len, dst_addr);
    
 
//    close(sockfd);
    return 0;
}

void *start_ping(void *argv)
{
	int rc;
	in_addr_t ipaddress = *(in_addr_t *)argv;
	pthread_t child;
	int i;
	struct sockaddr_in serv_name;
	struct hwaddr *HWaddr;	
	struct hostent *hp=NULL;
	HWaddr = (struct hwaddr *)malloc(sizeof(struct hwaddr));
	serv_name.sin_family = AF_INET;
	serv_name.sin_addr.s_addr = ipaddress;
	i = areq( (struct sockaddr *)&serv_name, sizeof(struct sockaddr), HWaddr);
	if( i == -2)
	{
		printf("\n Time out occured. May be invalid ip address");
		exit(1);
	}
	else if( i < 0 )
	{
		perror("Read");
		exit(1);
		
	}
	if ( i == 0)
	{

	while(1){
		pthread_mutex_lock(&index_mutex);
		if (stop_pinging){
			pthread_mutex_unlock(&index_mutex);
			goto out;
		}
		pthread_mutex_unlock(&index_mutex);
		hp = gethostbyaddr((char *) &serv_name.sin_addr.s_addr, sizeof(serv_name.sin_addr.s_addr), AF_INET);
		printf("\nping the %s with eth address :",hp->h_name); 
		for (i=0;i<8;i++)
		{
			printf("%02x", HWaddr->sll_addr[i]);
			if (i<7) printf(":");
		}
//		ping_prev_node();
		fflush(stdout);
		sleep(1);
		}

	}

	fflush(stdout);
out:	child = pthread_self();
	pthread_detach(child);
}




in_addr_t getipaddress(char *vmname)
{

	struct hostent *hptr;
	char str[LISTMAX], **pptr;
	in_addr_t ipaddress;

	if((hptr = gethostbyname(vmname))){
		switch(hptr->h_addrtype){
			case AF_INET: 
				pptr = hptr->h_addr_list;
				inet_ntop(hptr->h_addrtype, *pptr,str,sizeof(str));
//				printf("ip address is %s\n",str);
				ipaddress = inet_addr(str);
			break;
			default:
			printf("unknown address");break;
		}
		}
		else{printf("Invalid arguments");
			return -1;
			exit(1);
		}
	return ipaddress;


}


int start_tour(int rt_sock, struct touraddress *mynode) {
	
	struct iphdr *ip_hdr;
	int len, retval;
	struct sockaddr_in dest_addr;
	unsigned char *buf;

	dest_addr.sin_family = AF_INET;
	dest_addr.sin_addr.s_addr = mynode->tour_address[(mynode->curr_index)+1];

	len = sizeof(struct iphdr) + sizeof(struct touraddress);
	buf = (unsigned char *)malloc(len);
	memset(buf, 0, len);

	ip_hdr = (struct iphdr *)buf;

	ip_hdr->ihl = 5; 
	ip_hdr->version = IPVERSION;
	ip_hdr->tos = 0;
	ip_hdr->tot_len = htons(len);
	ip_hdr->id = htons(LOCAL_ID);
	ip_hdr->ttl = htons(TTL_OUT);
	ip_hdr->protocol = IPPROTO_TOUR;


	ip_hdr->saddr = mynode->tour_address[mynode->curr_index];
	ip_hdr->daddr = mynode->tour_address[(mynode->curr_index)+1];
	

	ip_hdr->check = in_cksum((unsigned short *)ip_hdr, len);
		
	memcpy(buf+sizeof(struct iphdr), mynode, sizeof(struct touraddress));
	
	retval = sendto(rt_sock, buf, len, 0, (struct sockaddr *)&dest_addr, sizeof(struct sockaddr));

	if (retval < 0) {
		printf("Error sending tour packet\n");
		return -1;
	}


fflush(stdout);
	

return 0;	

}


int route_packet_handler(int sockfd, int m_sock)
{
	int retval, i, len;
	char buf[MAXLINE];
	struct sockaddr_in src_addr;
	struct iphdr *ip_hdr;
	time_t t;
	struct touraddress *mynode;
	struct hostent *hptr;
	unsigned int previp;
	char msg[MAXLINE];
	pthread_t mythread[MAXLINE];
	in_addr_t peeripaddr;
	struct hostent *hp = NULL;
	
	mynode = (struct touraddress *) malloc(sizeof(struct touraddress));
	ip_hdr = (struct iphdr *) malloc(sizeof(struct iphdr));
	memset(buf, 0, MAXLINE);
	memset(msg, 0, MAXLINE);
	len = sizeof(src_addr);
	retval = recvfrom(sockfd, buf, MAXLINE, 0, (struct sockaddr *)&src_addr, &len); 

	if (retval < 0) {
		perror("\nrt recv");
		return -1;
	}

	ip_hdr = (struct iphdr *)buf;

	memcpy(mynode,buf+sizeof(struct iphdr), sizeof(struct touraddress));

	if (LOCAL_ID == ntohs(ip_hdr->id) ){
		time(&t);
		printf("\n%s", ctime(&t));
		
		hp = gethostbyaddr((char *) &src_addr.sin_addr.s_addr, sizeof(src_addr.sin_addr.s_addr), AF_INET);
		
		printf(" : received source routing packet from %s\n", hp->h_name);

fflush(stdout);

/* Check if the source is in your prev nodes list, if yes do not ping, else add to the list and ping */

		returnaddr = find_myprevaddr(mynode->tour_address[mynode->curr_index], head);

		if(!returnaddr)
		{
//			printf("\nNew node... so adding to the list of prev nodes");
			retval = add_myprevaddr(mynode->tour_address[mynode->curr_index], head);
			if (!retval){
				printf("\nError while adding to prev node list");
			}
			pthread_create(&mythread[counter], NULL, &start_ping, (void *)&mynode->tour_address[mynode->curr_index]);
			counter++;
		}

/* If first time visited add yourself to the multicast group */

		if ( first_visit )
		{
//			printf("\nfirst visit to the node. so, adding to multicast group");
			retval = join_multicast_group(m_sock, mynode->multicast_address, mynode->port);
		}
		

		if ( mynode->no_of_nodes == (mynode->curr_index)+1 )
		{
			sleep(5);
				
			sprintf(msg,"\nThis is node %s .  Tour has ended .  Group members please identify yourselves. ",myName);
			mcast_send(msg);
			printf("\nNode %s sending: %s",myName,msg);
		}
/* modify the packet and forward to the next node in the tour */
		else
		{
			mynode->curr_index++;
			start_tour(sockfd,mynode);
			
		}
		
	
	}
	else
		printf("\n Invalid packet Received");

fflush(stdout);
	
return 0;
}


int multicast_packet_handler (int sockfd)
{
	int retval, addrlen;
	char msgbuf[MAXLINE];
	struct sockaddr_in addr;

	memset(msgbuf, 0, MAXLINE);

	addrlen = sizeof(addr);
	if ((retval=recvfrom(sockfd,msgbuf,MAXLINE,0,(struct sockaddr *) &addr,&addrlen)) < 0) {
		perror("recvfrom");
		return -1;
	}
	second_multicast = 1;
	printf("\nNode %s Received: %s",myName,msgbuf);
	pthread_mutex_lock(&index_mutex);
	stop_pinging = 1;
	pthread_mutex_unlock(&index_mutex);
	sprintf(msgbuf,"Node %s. I am a member of the group",myName);
	printf("\nNode %s.Sending: %s",myName,msgbuf);
	mcast_send(msgbuf);
	
fflush(stdout);
return 0;
} 

int second_multicast_handler(int sockfd)
{
	int retval, addrlen,n;
	char msgbuf[MAXLINE];
	struct sockaddr_in addr;
	fd_set setallfd;
	struct timeval tv;

	tv.tv_sec = 5;
	tv.tv_usec = 0;
	

	memset(msgbuf, 0, MAXLINE);

	addrlen = sizeof(addr);

	while(1)
	{
		FD_ZERO(&setallfd);
		FD_SET(sockfd, &setallfd);
		n = select(sockfd + 1, &setallfd, NULL, NULL, &tv);
		if (n < 0) {
			printf("Error with select listening\n");
			return -1; 
		}
	
		if (FD_ISSET(sockfd, &setallfd)) {
			if ((retval=recvfrom(sockfd,msgbuf,MAXLINE,0,(struct sockaddr *) &addr,&addrlen)) < 0) {
				perror("recvfrom");
				return -1;
			}
			printf("\nNode %s Received: %s",myName,msgbuf);
		}
		if (n == 0){
			printf("\nTerminating the tour Application");
			break;
		}
	}

fflush(stdout);
exit(0);
return 0;
}

int pg_socket_handler()
{

	int n;
	char buffer[MAXLINE];
	struct iphdr* ip_reply;

	memset(buffer,0,MAXLINE);
	if (( n = recvfrom(pg_sock, buffer, sizeof(struct iphdr) + sizeof(struct icmphdr), 0, NULL, NULL)) == -1)
	{
		perror("recv");
	}
	else 
	{
	
    		ip_reply = (struct iphdr*) buffer;

		if (LOCAL_ID == ntohs(ip_reply->id) )
		{
			printf("Received %d byte\n", n);
	
		    printf("ID: %d\n", ntohs(ip_reply->id));
		    printf("TTL: %d\n", ip_reply->ttl);
		}		
	
	}
return 0;
}

int main(int argc, char **argv)
{

	int i, retval, rt_sock, pf_sock, maxfd, count =0;
	int on = 1;
	const int *val = &on;
	int optval;
	in_addr_t ipaddress,temp;
	struct touraddress *mynode;
	struct sockaddr_in multi_addr;
	fd_set setallfd;
	u_int yes=1;
	


	head = (struct myprevaddress *)malloc(sizeof(struct myprevaddress));

	mynode = (struct touraddress *)malloc(sizeof(struct touraddress));

	memset(&multi_addr,0,sizeof(multi_addr));


	rt_sock = socket(AF_INET, SOCK_RAW, IPPROTO_TOUR);
        if(rt_sock < 0){
                perror("\nError creating the route socket");
                return -1;
        }

        if(setsockopt(rt_sock, IPPROTO_IP, IP_HDRINCL, val, sizeof(on)) < 0) {
                perror("\nError in setsockopt");
		return -1;
        }

        /* IP Raw socket for ping messages */
        pg_sock = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	if(pg_sock < 0){
		perror("\nError creating ping RAW socket");
		return -1;
	}
	setsockopt(pg_sock, IPPROTO_IP, IP_HDRINCL, &optval, sizeof(int));

        /* Creating the PF_PACKET for sending ping messages */
        pf_sock = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_IP));
	if (pf_sock < 0) {
		printf("\nError in creating pf socket");
		return -1;
	}

	m_sock = socket(AF_INET, SOCK_DGRAM, 0);
        if(m_sock < 0) {
                printf("\nError in creating multicast sock");
                return -1;
        }

	m_readsock= socket(AF_INET, SOCK_DGRAM, 0);
	if(m_readsock < 0) {
		 printf("\nError in creating multicast sock");
                return -1;
        }	

	if (setsockopt(m_readsock,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(yes)) < 0) {
		perror("Reusing ADDR failed");
		return -1;
	}

	multi_addr.sin_family=AF_INET;
	multi_addr.sin_addr.s_addr=htonl(INADDR_ANY);
	multi_addr.sin_port=htons(MULTICAST_PORT);
     
     /* bind to receive address */
	if (bind(m_readsock,(struct sockaddr *) &multi_addr,sizeof(multi_addr)) < 0) {
		perror("Multicast read bind");
		return -1;
	}

	
	i = gethostname(myName,sizeof(myName));
	if ( i == -1)
	{
		printf("\nError in hostname");
		return -1;
	}


	if (argc > 1)
	{

		mynode->no_of_nodes = argc-1;
		
		mynode->tour_address[0] = (getipaddress(myName));
		for (i = 1; i < argc; i++){
			mynode->tour_address[i] = (getipaddress(argv[i]));
		}
	
		count = argc -1;
		for (i = 1; i <= count ; i++)
		{	
			temp = mynode->tour_address[i-1];
			if( temp == mynode->tour_address[i] ){
				printf("\n Invalid tour... Two consecutive nodes are equal ");
				return -1;
			}
		}
		mynode->curr_index = 0;
		mynode->port = MULTICAST_PORT;
		mynode->multicast_address = inet_addr(MULTICAST_ADDR);
		
		retval = join_multicast_group(m_sock, mynode->multicast_address, mynode->port);
		start_tour(rt_sock, mynode);
	}


	
	while (1) {
		FD_ZERO(&setallfd);
		FD_SET(rt_sock, &setallfd);
		FD_SET(pg_sock, &setallfd);
		FD_SET(m_readsock, &setallfd);

		if (m_readsock > rt_sock && m_readsock > pg_sock) {
			maxfd = m_readsock + 1;
		} 
		
		else if (pg_sock > rt_sock && pg_sock > m_readsock){
			maxfd = pg_sock + 1;
		}
		else
			maxfd = rt_sock + 1;


		retval = select(maxfd, &setallfd, NULL, NULL, NULL);
		if(retval < 0 )
		{
			printf("\nError occured in select"); 
		}

		if (FD_ISSET(rt_sock, &setallfd)) {
			route_packet_handler(rt_sock,m_sock);
		}
		if (FD_ISSET(pg_sock, &setallfd)) {
			//route_packet_handler(rt_sock);
			//pg_socket_handler();
		}
		if (FD_ISSET(m_readsock, &setallfd)) {
//			printf("\nIn m_socket select");
			if (second_multicast)
				second_multicast_handler(m_readsock);
			else
				multicast_packet_handler(m_readsock);
			
			fflush(stdout);
			//route_packet_handler(rt_sock);
		}

	}

return 0;
}

