
#include "arpheaders.h"


static struct hwa_info *hwa = NULL;
unsigned char my_mac_addr[6];
in_addr_t my_nonalias_ip_addr;
int connfd = -1;
int interface_index;

struct myaddress *head;
struct myaddress *myaddressnode;
struct mycache *cachehead;
struct mycache *mycachenode;

int add_myaddr(in_addr_t my_ip_addr, unsigned char my_hw_addr[6] , struct myaddress *head)
{
        if(!head->my_ip_addr)
        {
                head->my_ip_addr = my_ip_addr;
                memcpy(head->my_hw_addr, my_hw_addr,6);
                head->next = NULL;
                return 1;
        }
        else
        {
                struct myaddress *newnode = (struct myaddress *)malloc(sizeof(struct myaddress));
                struct myaddress *temp;
                newnode->my_ip_addr = my_ip_addr;
                memcpy(newnode->my_hw_addr, my_hw_addr,6);
                newnode->next = NULL;
                temp = head;
                while(temp->next)
                {
                        temp= temp->next;
                }
                temp->next = newnode;
                return 1;
        }
        return 0;
}

struct myaddress *find_myaddr(in_addr_t my_ip_addr, struct myaddress *head)
{
        struct myaddress *temp;
	temp = head;
	while(temp)
        {
		if(temp->my_ip_addr == my_ip_addr)
                {
                        return temp;
                }
                temp = temp->next;
	}

	return 0;
}

void print_myaddr(struct myaddress *head)
{
	struct myaddress *temp;
	int i;
	temp = head;
	while(temp)
	{
		printf("\nIP address is %s", inet_ntoa(*(struct in_addr *)&(temp->my_ip_addr)));
		printf("\t\tHW address is ");
		for (i=0;i<6;i++) {
		printf("%02x", temp->my_hw_addr[i]);
		if (i<5) printf(":");
		}
		
		temp = temp->next;
	}
fflush(stdout);
}


int add_mycache(in_addr_t my_ip_addr, unsigned char my_hw_addr[6] , int sll_ifiindex, uint8_t sll_hatype, int sockfd, struct mycache *head)
{
        if(!head->ip_addr)
        {
                head->ip_addr = my_ip_addr;
                memcpy(head->hw_addr, my_hw_addr,6);
		head->sll_ifiindex = sll_ifiindex;
		head->sll_hatype = sll_hatype;
		head->sockfd = sockfd;
                head->next = NULL;
                return 1;
        }
        else
        {
                struct mycache *newnode = (struct mycache *)malloc(sizeof(struct mycache));
                struct mycache *temp;
                newnode->ip_addr = my_ip_addr;
                memcpy(newnode->hw_addr, my_hw_addr,6);
		newnode->sll_ifiindex = sll_ifiindex;
		newnode->sll_hatype = sll_hatype;
		newnode->sockfd = sockfd;
                newnode->next = NULL;
                temp = head;
                while(temp->next)
                {
                        temp= temp->next;
                }
                temp->next = newnode;
                return 1;
        }
        return 0;
}

struct mycache *find_addr(in_addr_t my_ip_addr, struct mycache *head)
{
        struct mycache *temp;
	unsigned char default_mac[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
	temp = head;
	
	while(temp)
        {
//		if(temp->ip_addr == my_ip_addr && memcmp(temp->hw_addr, default_mac,6))
  		if(temp->ip_addr == my_ip_addr)
		{
                        return temp;
                }
                temp = temp->next;
	}

	return 0;
}

void print_mycache(struct mycache *head)
{
	struct mycache *temp;
	int i;
	temp = head;
	while(temp)
	{
		printf("\nIP Addr: %s", inet_ntoa(*(struct in_addr *)&(temp->ip_addr)));
		printf("\tHW addr: ");
		for (i=0;i<6;i++) {
		printf("%02x", temp->hw_addr[i]);
		if (i<5) printf(":");
		}
		printf("\tInterface: %d", temp->sll_ifiindex);
//		printf("\thatype: %x", temp->sll_hatype);
		printf("\tsocket: %d", temp->sockfd);
		
		temp = temp->next;
	}
}

int update_mycache(in_addr_t my_ip_addr, unsigned char my_hw_addr[6] , int sll_ifiindex, uint8_t sll_hatype, int sockfd, struct mycache *head, int socketupdate)
{
	struct mycache *temp;
	temp = head;
	while(temp)
        {
		if(temp->ip_addr == my_ip_addr)
                {
                        memcpy(temp->hw_addr, my_hw_addr,6);
			temp->sll_ifiindex = sll_ifiindex;
			temp->sll_hatype = sll_hatype;
//			if(socketupdate)
//				temp->sockfd = sockfd;
			return 1;
                }
                temp = temp->next;
	}

	return 0;
}


struct mycache *delete_from_cache(int sockfd, struct mycache *head)
{
        struct mycache *target = (struct mycache *)malloc(sizeof(struct mycache));
        struct mycache *prev = (struct mycache *)malloc(sizeof(struct mycache));
        
	target = head;

        while(target)
        {
                if(target->sockfd == sockfd)
                {
                        if(target == head && target->next == NULL)
                        {
				head = prev;
                                return head;
                        }
			else if (target == head)
			{
				head = head->next;
				return head;
			}
                        if(target->next)
                        {
                                prev->next = target->next;
                        }
                        else
                        {
                                prev->next = NULL;
                        }
                        return target;
                }
                prev = target;
                target = target->next;
        }
        return 0;
}

int update_socket(in_addr_t my_ip_addr, struct mycache *head)
{
	struct mycache *temp;
	temp = head;
	while(temp)
        {
		if(temp->ip_addr == my_ip_addr)
                {
			temp->sockfd = -1;
			return 1;
                }
                temp = temp->next;
	}

	return 0;
}


void printpackets(unsigned char dest_mac_address[6],unsigned char src_mac_address[6], struct arp_packet *pkt_to_be_sent)
{
	int i;

	printf("\nEthernet Header: \nDest Addr \t \t Source Addr \t \t Frame Type\n");

	for (i=0;i<6;i++) {
		printf("%02x", dest_mac_address[i]);
		if (i<5) printf(":");
	}
	printf("\t");
	for (i=0;i<6;i++) {
		printf("%02x", src_mac_address[i]);
		if (i<5) printf(":");
	}
	printf("\t%d",USID_PROTO);
	
	printf("\n\nArp Packet:");
	
	printf("\nhard_type : %04x \tproto_type : %04x \thard_size : %02x \tproto_size : %02x \toperation : %04x \tproto_num : %04x \t", pkt_to_be_sent->hard_type, pkt_to_be_sent->proto_type, pkt_to_be_sent->hard_size, pkt_to_be_sent->proto_size, pkt_to_be_sent->operation, pkt_to_be_sent->proto_num);
	printf("sender_eth_addr : ");
	for (i=0;i<6;i++) {
		printf("%02x", pkt_to_be_sent->sender_eth_addr[i]);
		if (i<5) printf(":");
	}
	printf("\tsender_ip_addr : %s",  inet_ntoa(*(struct in_addr *)&(pkt_to_be_sent->sender_ip_addr)));
	
	printf("\ttarget_eth_addr : ");
	for (i=0;i<6;i++) {
		printf("%02x", pkt_to_be_sent->target_eth_addr[i]);
		if (i<5) printf(":");
	}
	printf("\ttarget_ip_addr : %s",  inet_ntoa(*(struct in_addr *)&(pkt_to_be_sent->target_ip_addr)));

}
void send_Packet(int s, unsigned char src_mac_address[6], unsigned char dest_mac_address[6], int interface_index, struct arp_packet *pkt_to_be_sent)
{
	int i,j=0; /* Iterator*/
	
	/*target address*/
	struct sockaddr_ll socket_address;
	
	/*buffer for ethernet frame*/
	void* buffer = (void*)malloc(ETH_FRAME_LEN);
	 
	/*pointer to ethernet header*/
	unsigned char* etherhead = buffer;
		
	/*userdata in ethernet frame*/
	unsigned char* data = buffer + 14;
		
	/*another pointer to ethernet header*/
	struct ethhdr *eh = (struct ethhdr *)etherhead;
	 
	int send_result = 0;
	
	/*prepare sockaddr_ll*/
	
	/*RAW communication*/
	socket_address.sll_family   = PF_PACKET;	
	/*we don't use a protocoll above ethernet layer
	  ->just use anything here*/
	socket_address.sll_protocol = htons(USID_PROTO);	
	
	/*index of the network device
	see full code later how to retrieve it*/
	socket_address.sll_ifindex  = interface_index;
	
	/*ARP hardware identifier is ethernet*/
	socket_address.sll_hatype   = ARPHRD_ETHER;
		
	/*target is another host*/
	socket_address.sll_pkttype  = PACKET_OTHERHOST;
	
	/*address length*/
	socket_address.sll_halen    = ETH_ALEN;		
	/*MAC - begin*/
	socket_address.sll_addr[0]  = dest_mac_address[0];		
	socket_address.sll_addr[1]  = dest_mac_address[1];		
	socket_address.sll_addr[2]  = dest_mac_address[2];
	socket_address.sll_addr[3]  = dest_mac_address[3];
	socket_address.sll_addr[4]  = dest_mac_address[4];
	socket_address.sll_addr[5]  = dest_mac_address[5];
	/*MAC - end*/
	socket_address.sll_addr[6]  = 0x00;/*not used*/
	socket_address.sll_addr[7]  = 0x00;/*not used*/
	
	
	/*set the frame header*/
	memcpy((void*)buffer, (void*)dest_mac_address, ETH_ALEN);
	memcpy((void*)(buffer+ETH_ALEN), (void*)src_mac_address, ETH_ALEN);
	memcpy((void*)(buffer+ETH_ALEN+ETH_ALEN+2), (struct arp_packet*) pkt_to_be_sent , sizeof(struct arp_packet));
	eh->h_proto = htons(USID_PROTO);	
	printf("\n\nSending Packet is\n");
	printpackets(dest_mac_address, src_mac_address, pkt_to_be_sent);


	/*send the packet*/
	send_result = sendto(s, buffer, ETH_FRAME_LEN, 0, 
		      (struct sockaddr*)&socket_address, sizeof(socket_address));
	if (send_result == -1) 
	{
		perror("sendto() failed");
		exit(1);
	}
	
}


int packet_process(int sockfd, struct sockaddr_ll socket_address, char buffer[MAXLINE])
{
	int i, iamdest = 0, cachesockfd;
	struct arp_packet *test_packet;
	unsigned char destination_mac[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
	unsigned char temp_hw_addr[6], temp1_hw_addr[6];
	struct hwaddr *HWaddr;
	in_addr_t temp_ip_addr;

	HWaddr = (struct hwaddr *)malloc(sizeof(struct hwaddr));
	test_packet = (struct arp_packet *)malloc(sizeof(struct arp_packet));

	memcpy(temp_hw_addr, (unsigned char *)buffer, ETH_ALEN);
	memcpy(temp1_hw_addr, (unsigned char *)buffer+ETH_ALEN, ETH_ALEN);
	test_packet = (struct arp_packet *)(buffer+14);
//	printf("\n sender ip address is %s\n", inet_ntoa(*(struct in_addr *)&(test_packet->sender_ip_addr)));

/* If the packet is request packet, check if i am destination (send reply, update or add cache). If not, update my cache if the source ip already present. DONT ADD NEW ENTRY */

	myaddressnode = find_myaddr(test_packet->target_ip_addr, head);


/* Check if i am destination */	
	if (myaddressnode){

		iamdest = 1;
	}
	else
	{
		printf("\nI am not the destination");
	}
/* end of destination check code */


	if(iamdest){
		mycachenode = find_addr(test_packet->sender_ip_addr, cachehead);

		if(mycachenode){
			if(test_packet->operation == REQ_OP)
				update_mycache(test_packet->sender_ip_addr, test_packet->sender_eth_addr , socket_address.sll_ifindex, socket_address.sll_hatype, -1, cachehead, 1);
			else{
				update_mycache(test_packet->sender_ip_addr, test_packet->sender_eth_addr , socket_address.sll_ifindex, socket_address.sll_hatype, -1, cachehead, 0);
			}

		}
		else{
			add_mycache(test_packet->sender_ip_addr, test_packet->sender_eth_addr , socket_address.sll_ifindex, socket_address.sll_hatype, -1, cachehead);
			
		}
	}

	else{
		mycachenode = find_addr(test_packet->sender_ip_addr, cachehead);
		if(mycachenode){
			update_mycache(test_packet->sender_ip_addr, test_packet->sender_eth_addr , socket_address.sll_ifindex, socket_address.sll_hatype, -1, cachehead, 1);
		}

	}

/* End of update or adding of cache block */


	if(test_packet->operation == REQ_OP)
	{

		if (iamdest){
			printf("\n\nReceived Packet is\n");
			printpackets(temp_hw_addr,temp1_hw_addr, test_packet);

			/* exchange source and destination fields and send the reply */
			temp_ip_addr = test_packet->target_ip_addr;
			test_packet->target_ip_addr = test_packet->sender_ip_addr;
			test_packet->sender_ip_addr = temp_ip_addr;
			test_packet->operation = RES_OP;

			memcpy(test_packet->target_eth_addr, test_packet->sender_eth_addr,HARD_SIZE);
			memcpy(test_packet->sender_eth_addr, myaddressnode->my_hw_addr,HARD_SIZE);

			send_Packet(sockfd, myaddressnode->my_hw_addr, test_packet->target_eth_addr, interface_index, test_packet);

		}
	}
	else if (test_packet->operation == RES_OP)
	{
		
		if (iamdest){
			printf("\nReceived Packet is\n");
			printpackets(temp_hw_addr,temp1_hw_addr, test_packet);

			mycachenode = find_addr(test_packet->sender_ip_addr, cachehead);
			if(mycachenode){
				HWaddr->sll_ifindex = mycachenode->sll_ifiindex;
				HWaddr->sll_hatype = (unsigned char)mycachenode->sll_hatype;
				memcpy(HWaddr->sll_addr, mycachenode->hw_addr,6);
				HWaddr->sll_addr[6] = 0;
				HWaddr->sll_addr[7] = 0;
				HWaddr->sll_halen = 8;			
				if ( (write(mycachenode->sockfd, HWaddr,sizeof(struct hwaddr) )) < 0 ){
					perror("\nwrite in pf handler");
					exit(1);
				}
				//update_socket(test_packet->sender_ip_addr, cachehead);
			}
		}
	}
	else{
		printf("\nUnknown packet");
	}
	
return 0;
}

int areq_process(int connfd1, int pf_sock, char *buf, struct sockaddr_un cli_addr)
{
	int n,i, ret;
	struct hwaddr *HWaddr = (struct hwaddr *)malloc(sizeof(struct hwaddr));
	struct sockaddr *IPaddr = (struct sockaddr *)malloc(sizeof(struct sockaddr));
	struct sockaddr_in *IPaddress = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in));
	in_addr_t ip_addr;
	unsigned char hw_addr[6];
	unsigned char destination_mac[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
	struct arp_packet *test_packet = (struct arp_packet *)malloc(sizeof(struct arp_packet));


	IPaddr = (struct sockaddr *)buf;
        HWaddr = (struct hwaddr *)(buf + sizeof(struct sockaddr_in));
	IPaddress = (struct sockaddr_in *)IPaddr;
	ip_addr = IPaddress->sin_addr.s_addr;

/* check if the hw address is present in the local cache */
	mycachenode = find_addr(ip_addr, cachehead);
	if(mycachenode){
		printf("\nAddress present in my cache, so replying");
		
		HWaddr->sll_ifindex = mycachenode->sll_ifiindex;
		HWaddr->sll_hatype = (unsigned char) mycachenode->sll_hatype;
		HWaddr->sll_halen = 8;
		memcpy(HWaddr->sll_addr, mycachenode->hw_addr, HARD_SIZE);
		HWaddr->sll_addr[6]=0;
		HWaddr->sll_addr[7]=0;

		if(write(connfd1, HWaddr, sizeof(struct hwaddr)) < 0) {
			perror("\n Error occured while writing to unix domain socket");
			exit(1);
		}
		//update_socket(ip_addr, cachehead);
		close(connfd1);
	}
	
/* if not in cache, send a arp request through the PF_Packet */
	else{
		printf("\n Address not found in local cache");
		/* Add an incomplete details into the cache */
		n = add_mycache(ip_addr, destination_mac , interface_index, ARPHRD_ETHER, connfd1, cachehead);
		if ( n < 0 )
		{
			printf("\nError while adding the incomplete entry to cache");
		}

		/* Send a request on PF_SOCKET */
		test_packet->hard_type = HARD_TYPE;
		test_packet->proto_type = PROTO_TYPE;
		test_packet->hard_size = HARD_SIZE;
		test_packet->proto_size = PROTO_SIZE;
		test_packet->operation = REQ_OP; // request
		test_packet->proto_num = htons(ARP_PROTO);	
		test_packet->sender_ip_addr = my_nonalias_ip_addr;

		memcpy(test_packet->sender_eth_addr, my_mac_addr,6);

		test_packet->target_ip_addr = ip_addr;
		
		send_Packet(pf_sock, my_mac_addr, destination_mac, interface_index, test_packet);		
		
	}
				

}

int main(int argc, char *argv[])
{
	
	int pf_sock, unx_sock, j, send_result, recv_len, ret_val=0;
	unsigned char src_mac[6], dest_mac[6];	
	in_addr_t ip_addr, dest_ip_addr;
	unsigned char buf[MAXLINE],tempbuffer[MAXLINE], hw_addr[6];
	struct sockaddr_un serv_addr, cli_addr;
	struct sockaddr_ll socket_address;
	struct sockaddr *sa;
	char name[20];
	char *ptr;
	int length, len, maxfd,i,prflag;
	fd_set rset;

	head = (struct myaddress *)malloc(sizeof(struct myaddress));
	cachehead = (struct mycache *)malloc(sizeof(struct mycache));
	memset(buf,0,MAXLINE);
	memset(tempbuffer,0,MAXLINE);	
	hwa = Get_hw_addrs();

	for (;hwa != NULL; hwa = hwa->hwa_next){
		if( !strncmp(hwa->if_name,"eth0",4) ){
			
			if ( (sa = hwa->ip_addr) != NULL){
		//		printf("\n         IP addr = %s ", Sock_ntop_host(sa, sizeof(*sa)));
				ip_addr = inet_addr(Sock_ntop_host(sa, sizeof(*sa)));
			

				memcpy(hw_addr,(unsigned char*)hwa->if_haddr,HARD_SIZE);
				
				/* store my mac and ip address of eth0 */
				if(!strcmp(hwa->if_name,"eth0")){
					memcpy(my_mac_addr,hw_addr,HARD_SIZE);
					my_nonalias_ip_addr = ip_addr;
					interface_index = hwa->if_index;
				}

		
				ret_val = add_myaddr(ip_addr, hw_addr , head);
			
				if(ret_val == 0){
					printf("\nError in adding to the table");
					return 0;
				}
			}
		
		}
	}
	printf("\n My IP and HW Addresses are:");
	print_myaddr(head);
	/* Unix domain SOCK_STREAM socket */	
	unlink(ARP_PATH);
	unx_sock = socket(AF_LOCAL, SOCK_STREAM, 0);
	
	bzero(&serv_addr, sizeof(serv_addr));
	serv_addr.sun_family = AF_LOCAL;
	strcpy(serv_addr.sun_path, ARP_PATH);

	length = sizeof(serv_addr);
	if (bind(unx_sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
		printf("Error in binding unix domain socket\n");
		return -1;
	}

	listen(unx_sock, 10);

	/* PF_PACKET socket */	
	pf_sock = socket(PF_PACKET, SOCK_RAW, htons(USID_PROTO));
	if (pf_sock < 0) {
		printf("Error in getting pf socket descriptor\n");
		return -1;
	}
	
	FD_ZERO(&rset);
	
	while (1) {
		FD_SET(pf_sock, &rset);
		FD_SET(unx_sock, &rset);

		if (pf_sock > unx_sock) {
			maxfd = pf_sock + 1;
		} else {
			maxfd = unx_sock + 1;
		}
		
	
		select(maxfd, &rset, NULL, NULL, NULL);
		if (FD_ISSET(pf_sock, &rset)) {
			len = sizeof(socket_address);
			recv_len = recvfrom(pf_sock, buf, ETH_FRAME_LEN, 0, (struct sockaddr *)&socket_address, &len);
			if (recv_len < 0) {
				perror("");
				printf("\nError in reading data from pf socket\n");
				exit(1);
			}

			packet_process(pf_sock,socket_address,buf);
			
		}

		if (FD_ISSET(unx_sock, &rset)) {
			length = sizeof(cli_addr);
			connfd = accept(unx_sock, (struct sockaddr *) &cli_addr, &length);
			recv_len = recvfrom(connfd, buf, MAXLINE, 0, NULL, NULL);
			
			if (recv_len < 0) {
				printf("Error in reading data from unix domain socket\n");
				return -1;
			}	
			areq_process(connfd, pf_sock, buf, cli_addr);		
			//send_data_packet(pf_socket, buf, unx_socket_address.sun_path);			
		}

		printf("\n");
	}
	return 1;
}

